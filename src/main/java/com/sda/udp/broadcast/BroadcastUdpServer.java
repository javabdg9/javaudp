package com.sda.udp.broadcast;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;

public class BroadcastUdpServer {
    final static String INET_ADDR = "255.255.255.255";
    final static int PORT = 11222;

    public static void main(String[] args) throws UnknownHostException {
        InetAddress address = InetAddress.getByName(INET_ADDR);

        byte[] buf = new byte[256];

        try (MulticastSocket clientSocket = new MulticastSocket(PORT)) {
            System.out.println("Serwer nasłuchuje w trybie broadcast");
            while (true) {
                DatagramPacket msgPacket = new DatagramPacket(buf, buf.length);
                clientSocket.receive(msgPacket);

                String msg = new String(buf, 0, msgPacket.getLength());
                System.out.println("Server received msg: " + msg);
                if(msg.equals("end")){
                    System.out.print("Server closing...");
                    break;
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        System.out.print("ok");
    }
}
