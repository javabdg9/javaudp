package com.sda.udp.mutlicast;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Scanner;

public class MulticastUdpClient {
    final static String INET_ADDR = "224.0.0.3";
    final static int PORT = 8888;

    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Program umożliwia wysyłanie wiadomości typu multicast na " + INET_ADDR + ":" + PORT);
        String message = "";
        while (!message.equals("exit")) {
            System.out.print("Wprowadź wiadomość do wysłania: ");
            message = scanner.nextLine();
            System.out.println("Send: " + sendMessage(message));
        }
    }

    static boolean sendMessage(String message) throws UnknownHostException {
        InetAddress addr = InetAddress.getByName(INET_ADDR);
        try (DatagramSocket serverSocket = new DatagramSocket()) {
            DatagramPacket msgPacket = new DatagramPacket(message.getBytes(), message.getBytes().length, addr, PORT);
            serverSocket.send(msgPacket);
            return true;
        } catch (IOException ex) {
            return false;
        }
    }
}
