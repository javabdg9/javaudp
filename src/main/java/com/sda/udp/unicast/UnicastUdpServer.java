package com.sda.udp.unicast;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class UnicastUdpServer {
	final static String INET_ADDR = "192.168.0.32";
	final static int PORT = 12345;

	public static void main(String[] args) throws UnknownHostException {
		InetAddress address = InetAddress.getByName(INET_ADDR);

		byte[] buf = new byte[256];

		try (DatagramSocket clientSocket = new DatagramSocket(PORT)) {
			while (true) {
				DatagramPacket msgPacket = new DatagramPacket(buf, buf.length);
				clientSocket.receive(msgPacket);

                String msg = new String(buf, 0, msgPacket.getLength());
                System.out.println("Server received msg: " + msg);
                if(msg.equals("end")){
                    System.out.print("Server closing...");
                    break;
                }
			}
		}
		catch (IOException ex) {
			ex.printStackTrace();
		}
	}
}
